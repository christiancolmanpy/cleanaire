//
//  Configuracion.swift
//  aire
//
//  Created by Christian Colman on 2023-03-31.
//

import SwiftUI

struct Configuracion: View {
    var body: some View {
        ZStack {
            Color.blue
            Text(/*@START_MENU_TOKEN@*/"Configuracion"/*@END_MENU_TOKEN@*/)
        }
    }
}

struct Configuracion_Previews: PreviewProvider {
    static var previews: some View {
        Configuracion()
    }
}
