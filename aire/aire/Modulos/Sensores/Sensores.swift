//
//  Sensores.swift
//  aire
//
//  Created by Christian Colman on 2023-03-31.
//

import SwiftUI

struct Sensores: View {
    var body: some View {
        ZStack {
            Color.green
            Text(/*@START_MENU_TOKEN@*/"Sensores"/*@END_MENU_TOKEN@*/)
        }
    }
}

struct Sensores_Previews: PreviewProvider {
    static var previews: some View {
        Sensores()
    }
}
