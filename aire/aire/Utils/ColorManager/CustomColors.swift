//
//  CustomColors.swift
//  aire
//
//  Created by Christian Colman on 2023-03-31.
//

import SwiftUI

extension UIColor {
    static let celesteClaro = UIColor(red: 226/255, green: 243/255, blue: 245/255, alpha: 1)
    static let celesteOscuro = UIColor(red: 34/255, green: 209/255, blue: 238/255, alpha: 1)
    static let azulClaro = UIColor(red: 61/255, green: 90/255, blue: 241/255, alpha: 1)
    static let azulOscuro = UIColor(red: 14/255, green: 21/255, blue: 58/255, alpha: 1)
}
