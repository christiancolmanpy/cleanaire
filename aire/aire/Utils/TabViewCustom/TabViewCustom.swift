//
//  TabViewCustom.swift
//  aire
//
//  Created by Christian Colman on 2023-03-31.
//

import SwiftUI

struct TabViewCustom: View {
    var body: some View {
        TabView {
            Nosotros().tabItem{
                Image(systemName: "house.fill")
                Text("Nosotros")
            }
            Sensores().tabItem{
                Image(systemName: "house.fill")
                Text("Mapa")
            }
            Configuracion().tabItem{
                Image(systemName: "house.fill")
                Text("Configuracion")
            }
        }
        .onAppear() {
            UITabBar.appearance().backgroundColor = UIColor.celesteOscuro
        }
        .tint(.white)
        
    }
}

struct TabViewCustom_Previews: PreviewProvider {
    static var previews: some View {
        TabViewCustom()
    }
}
