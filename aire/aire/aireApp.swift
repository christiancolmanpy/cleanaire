//
//  aireApp.swift
//  aire
//
//  Created by Christian Colman on 2023-03-31.
//

import SwiftUI

@main
struct aireApp: App {
    var body: some Scene {
        WindowGroup {
            TabViewCustom()
        }
    }
}
